package com.oriente.meatloaf.model.midtrans;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.validation.Valid;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "status_code",
        "status_message",
        "transaction_id",
        "order_id",
        "gross_amount",
        "payment_type",
        "transaction_time",
        "transaction_status",
        "fraud_status",
        "permata_va_number",
        "signature_key",
        "va_numbers",
        "bill_key",
        "biller_code"
})
public class MidtransResponse {

    @JsonProperty("status_code")
    private String statusCode;
    @JsonProperty("status_message")
    private String statusMessage;
    @JsonProperty("transaction_id")
    private String transactionId;
    @JsonProperty("order_id")
    private String orderId;
    @JsonProperty("gross_amount")
    private String grossAmount;
    @JsonProperty("payment_type")
    private String paymentType;
    @JsonProperty("transaction_time")
    private String transactionTime;
    @JsonProperty("transaction_status")
    private String transactionStatus;
    @JsonProperty("fraud_status")
    private String fraudStatus;
    @JsonProperty("permata_va_number")
    private String permataVaNumber;
    @JsonProperty("signature_key")
    private String signatureKey;
    @JsonProperty("va_numbers")
    @Valid
    private List<VaNumber> vaNumbers = null;
    @JsonProperty("bill_key")
    private String billKey;
    @JsonProperty("biller_code")
    private String billerCode;
    @JsonIgnore
    @Valid
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * No args constructor for use in serialization
     *
     */
    public MidtransResponse() {
    }

    /**
     *
     * @param billKey
     * @param transactionId
     * @param paymentType
     * @param billerCode
     * @param signatureKey
     * @param grossAmount
     * @param fraudStatus
     * @param statusMessage
     * @param statusCode
     * @param transactionTime
     * @param vaNumbers
     * @param transactionStatus
     * @param orderId
     * @param permataVaNumber
     */
    public MidtransResponse(String statusCode, String statusMessage, String transactionId, String orderId, String grossAmount, String paymentType, String transactionTime, String transactionStatus, String fraudStatus, String permataVaNumber, String signatureKey, List<VaNumber> vaNumbers, String billKey, String billerCode) {
        super();
        this.statusCode = statusCode;
        this.statusMessage = statusMessage;
        this.transactionId = transactionId;
        this.orderId = orderId;
        this.grossAmount = grossAmount;
        this.paymentType = paymentType;
        this.transactionTime = transactionTime;
        this.transactionStatus = transactionStatus;
        this.fraudStatus = fraudStatus;
        this.permataVaNumber = permataVaNumber;
        this.signatureKey = signatureKey;
        this.vaNumbers = vaNumbers;
        this.billKey = billKey;
        this.billerCode = billerCode;
    }

    @JsonProperty("status_code")
    public String getStatusCode() {
        return statusCode;
    }

    @JsonProperty("status_code")
    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public MidtransResponse withStatusCode(String statusCode) {
        this.statusCode = statusCode;
        return this;
    }

    @JsonProperty("status_message")
    public String getStatusMessage() {
        return statusMessage;
    }

    @JsonProperty("status_message")
    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

    public MidtransResponse withStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
        return this;
    }

    @JsonProperty("transaction_id")
    public String getTransactionId() {
        return transactionId;
    }

    @JsonProperty("transaction_id")
    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public MidtransResponse withTransactionId(String transactionId) {
        this.transactionId = transactionId;
        return this;
    }

    @JsonProperty("order_id")
    public String getOrderId() {
        return orderId;
    }

    @JsonProperty("order_id")
    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public MidtransResponse withOrderId(String orderId) {
        this.orderId = orderId;
        return this;
    }

    @JsonProperty("gross_amount")
    public String getGrossAmount() {
        return grossAmount;
    }

    @JsonProperty("gross_amount")
    public void setGrossAmount(String grossAmount) {
        this.grossAmount = grossAmount;
    }

    public MidtransResponse withGrossAmount(String grossAmount) {
        this.grossAmount = grossAmount;
        return this;
    }

    @JsonProperty("payment_type")
    public String getPaymentType() {
        return paymentType;
    }

    @JsonProperty("payment_type")
    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public MidtransResponse withPaymentType(String paymentType) {
        this.paymentType = paymentType;
        return this;
    }

    @JsonProperty("transaction_time")
    public String getTransactionTime() {
        return transactionTime;
    }

    @JsonProperty("transaction_time")
    public void setTransactionTime(String transactionTime) {
        this.transactionTime = transactionTime;
    }

    public MidtransResponse withTransactionTime(String transactionTime) {
        this.transactionTime = transactionTime;
        return this;
    }

    @JsonProperty("transaction_status")
    public String getTransactionStatus() {
        return transactionStatus;
    }

    @JsonProperty("transaction_status")
    public void setTransactionStatus(String transactionStatus) {
        this.transactionStatus = transactionStatus;
    }

    public MidtransResponse withTransactionStatus(String transactionStatus) {
        this.transactionStatus = transactionStatus;
        return this;
    }

    @JsonProperty("fraud_status")
    public String getFraudStatus() {
        return fraudStatus;
    }

    @JsonProperty("fraud_status")
    public void setFraudStatus(String fraudStatus) {
        this.fraudStatus = fraudStatus;
    }

    public MidtransResponse withFraudStatus(String fraudStatus) {
        this.fraudStatus = fraudStatus;
        return this;
    }

    @JsonProperty("permata_va_number")
    public String getPermataVaNumber() {
        return permataVaNumber;
    }

    @JsonProperty("permata_va_number")
    public void setPermataVaNumber(String permataVaNumber) {
        this.permataVaNumber = permataVaNumber;
    }

    public MidtransResponse withPermataVaNumber(String permataVaNumber) {
        this.permataVaNumber = permataVaNumber;
        return this;
    }

    @JsonProperty("signature_key")
    public String getSignatureKey() {
        return signatureKey;
    }

    @JsonProperty("signature_key")
    public void setSignatureKey(String signatureKey) {
        this.signatureKey = signatureKey;
    }

    public MidtransResponse withSignatureKey(String signatureKey) {
        this.signatureKey = signatureKey;
        return this;
    }

    @JsonProperty("va_numbers")
    public List<VaNumber> getVaNumbers() {
        return vaNumbers;
    }

    @JsonProperty("va_numbers")
    public void setVaNumbers(List<VaNumber> vaNumbers) {
        this.vaNumbers = vaNumbers;
    }

    public MidtransResponse withVaNumbers(List<VaNumber> vaNumbers) {
        this.vaNumbers = vaNumbers;
        return this;
    }

    @JsonProperty("bill_key")
    public String getBillKey() {
        return billKey;
    }

    @JsonProperty("bill_key")
    public void setBillKey(String billKey) {
        this.billKey = billKey;
    }

    public MidtransResponse withBillKey(String billKey) {
        this.billKey = billKey;
        return this;
    }

    @JsonProperty("biller_code")
    public String getBillerCode() {
        return billerCode;
    }

    @JsonProperty("biller_code")
    public void setBillerCode(String billerCode) {
        this.billerCode = billerCode;
    }

    public MidtransResponse withBillerCode(String billerCode) {
        this.billerCode = billerCode;
        return this;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public MidtransResponse withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("statusCode", statusCode).append("statusMessage", statusMessage).append("transactionId", transactionId).append("orderId", orderId).append("grossAmount", grossAmount).append("paymentType", paymentType).append("transactionTime", transactionTime).append("transactionStatus", transactionStatus).append("fraudStatus", fraudStatus).append("permataVaNumber", permataVaNumber).append("signatureKey", signatureKey).append("vaNumbers", vaNumbers).append("billKey", billKey).append("billerCode", billerCode).append("additionalProperties", additionalProperties).toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(billKey).append(transactionId).append(paymentType).append(billerCode).append(signatureKey).append(grossAmount).append(fraudStatus).append(statusMessage).append(statusCode).append(transactionTime).append(additionalProperties).append(vaNumbers).append(transactionStatus).append(orderId).append(permataVaNumber).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof MidtransResponse) == false) {
            return false;
        }
        MidtransResponse rhs = ((MidtransResponse) other);
        return new EqualsBuilder().append(billKey, rhs.billKey).append(transactionId, rhs.transactionId).append(paymentType, rhs.paymentType).append(billerCode, rhs.billerCode).append(signatureKey, rhs.signatureKey).append(grossAmount, rhs.grossAmount).append(fraudStatus, rhs.fraudStatus).append(statusMessage, rhs.statusMessage).append(statusCode, rhs.statusCode).append(transactionTime, rhs.transactionTime).append(additionalProperties, rhs.additionalProperties).append(vaNumbers, rhs.vaNumbers).append(transactionStatus, rhs.transactionStatus).append(orderId, rhs.orderId).append(permataVaNumber, rhs.permataVaNumber).isEquals();
    }

}