
package com.oriente.meatloaf.model.doku.inquiry;

import java.util.HashMap;
import java.util.Map;
import javax.validation.Valid;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "origin",
    "destination",
    "fees"
})
public class Fund {

    @JsonProperty("origin")
    @Valid
    private Origin origin;
    @JsonProperty("destination")
    @Valid
    private Destination destination;
    @JsonProperty("fees")
    @Valid
    private Fees fees;
    @JsonIgnore
    @Valid
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * No args constructor for use in serialization
     * 
     */
    public Fund() {
    }

    /**
     * 
     * @param origin
     * @param fees
     * @param destination
     */
    public Fund(Origin origin, Destination destination, Fees fees) {
        super();
        this.origin = origin;
        this.destination = destination;
        this.fees = fees;
    }

    @JsonProperty("origin")
    public Origin getOrigin() {
        return origin;
    }

    @JsonProperty("origin")
    public void setOrigin(Origin origin) {
        this.origin = origin;
    }

    public Fund withOrigin(Origin origin) {
        this.origin = origin;
        return this;
    }

    @JsonProperty("destination")
    public Destination getDestination() {
        return destination;
    }

    @JsonProperty("destination")
    public void setDestination(Destination destination) {
        this.destination = destination;
    }

    public Fund withDestination(Destination destination) {
        this.destination = destination;
        return this;
    }

    @JsonProperty("fees")
    public Fees getFees() {
        return fees;
    }

    @JsonProperty("fees")
    public void setFees(Fees fees) {
        this.fees = fees;
    }

    public Fund withFees(Fees fees) {
        this.fees = fees;
        return this;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public Fund withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("origin", origin).append("destination", destination).append("fees", fees).append("additionalProperties", additionalProperties).toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(additionalProperties).append(origin).append(fees).append(destination).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof Fund) == false) {
            return false;
        }
        Fund rhs = ((Fund) other);
        return new EqualsBuilder().append(additionalProperties, rhs.additionalProperties).append(origin, rhs.origin).append(fees, rhs.fees).append(destination, rhs.destination).isEquals();
    }

}
