package com.oriente.meatloaf.model.midtrans.bca;

import java.util.HashMap;
import java.util.Map;
import javax.validation.Valid;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "sub_company_code"
})
public class Bca {

    @JsonProperty("sub_company_code")
    private String subCompanyCode;
    @JsonIgnore
    @Valid
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * No args constructor for use in serialization
     *
     */
    public Bca() {
    }

    /**
     *
     * @param subCompanyCode
     */
    public Bca(String subCompanyCode) {
        super();
        this.subCompanyCode = subCompanyCode;
    }

    @JsonProperty("sub_company_code")
    public String getSubCompanyCode() {
        return subCompanyCode;
    }

    @JsonProperty("sub_company_code")
    public void setSubCompanyCode(String subCompanyCode) {
        this.subCompanyCode = subCompanyCode;
    }

    public Bca withSubCompanyCode(String subCompanyCode) {
        this.subCompanyCode = subCompanyCode;
        return this;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public Bca withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("subCompanyCode", subCompanyCode).append("additionalProperties", additionalProperties).toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(additionalProperties).append(subCompanyCode).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof Bca) == false) {
            return false;
        }
        Bca rhs = ((Bca) other);
        return new EqualsBuilder().append(additionalProperties, rhs.additionalProperties).append(subCompanyCode, rhs.subCompanyCode).isEquals();
    }

}