package com.oriente.meatloaf.web;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import com.oriente.meatloaf.model.doku.disbursement.DokuDisbursementRequest;
import com.oriente.meatloaf.model.doku.disbursement.DokuDisbursementResponse;
import com.oriente.meatloaf.model.doku.inquiry.DokuInquiryResponse;
import com.oriente.meatloaf.repositories.CwmPaymentRepository;

import org.apache.tomcat.util.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Formatter;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;


@RestController
public class RootController {

    private static final Logger logger = LoggerFactory.getLogger(RootController.class);

    private static String dokuAgentKey = "A40825";  // for doku disbursement API
    private static String dokuMallId = "5561";      // for doku VA API
    private static String dokuSharedKey = "g57Gc4vFV3eM";

    private static String doku_va_url = "https://staging.doku.com/api/payment/doGeneratePaymentCode";
    private static String doku_disburse_url = "https://staging.doku.com/apikirimdoku";

    private static String meatloaf_paid_notification_url = ""; // url to notify paid va: volt/diplopod

    private static String doku_encKey = "kiqskg32qjvfdh70";
    private static String ALGORITHM = "AES";

    private RestTemplate restTemplate = new RestTemplate(getClientHttpRequestFactory());

    private ClientHttpRequestFactory getClientHttpRequestFactory() {
        HttpComponentsClientHttpRequestFactory clientHttpRequestFactory = new HttpComponentsClientHttpRequestFactory();
        clientHttpRequestFactory.setConnectTimeout(2000);
        clientHttpRequestFactory.setReadTimeout(2000);
        return clientHttpRequestFactory;
    }

    @Autowired
    CwmPaymentRepository cwmPaymentRepo;


    // receive & process response from doku
    // for va paid notification, had to be registered to doku
    @RequestMapping(value = "/doku", method = RequestMethod.POST)
    public String doku(@RequestBody String body) {
        logger.info("Receiving: " + body);
        String[] lines = body.split("&");

        ObjectMapper mapper = new ObjectMapper();
        ObjectNode node = mapper.createObjectNode();
        for (String line : lines) {
            String[] kv = line.split("=");
            if (kv.length == 1) {
                logger.info(kv[0] + ": ");
                node.put(kv[0], "");
            }
            else {
                logger.info(kv[0] + ": " + kv[1]);
                node.put(kv[0], kv[1]);
            }
        }

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> entity = new HttpEntity<String>(node.toString(), headers);
        ResponseEntity<String> responseEntity = restTemplate.exchange(meatloaf_paid_notification_url, HttpMethod.POST, entity, String.class);
        logger.info("paid va notification response: " + responseEntity.getStatusCodeValue());
        return "Continue";
    }

    // doku va: request for paycode
    @RequestMapping(value = "/doku/payIn", method = RequestMethod.POST)
    public ResponseEntity<?> payIn(@RequestBody String inputString) throws IOException {
        Map outputMap = new HashMap<>();
        String message = "payIn failed";
        int status = 1;
        HttpStatus responseCode = HttpStatus.INTERNAL_SERVER_ERROR;

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
        Date date = new Date();
        String reqDateTime = dateFormat.format(date);
        String sessionId = generateSHA1(reqDateTime);

        ObjectMapper mapper = new ObjectMapper();
        JsonNode jsonNode = mapper.readTree(inputString);
        ObjectNode node = jsonNode.deepCopy();
        node.put("req_mall_id", dokuMallId);
        node.put("req_chain_merchant", "NA");
        //node.put("req_amount", amount);

        String amount = jsonNode.get("req_amount").asText();
        String transId = jsonNode.get("req_trans_id_merchant").asText();

        String words = amount + dokuMallId + dokuSharedKey + transId + "360";
        node.put("req_words", generateSHA1(words));
        node.put("req_trans_id_merchant", transId);
        node.put("req_purchase_amount", amount);
        node.put("req_request_date_time", reqDateTime);
        node.put("req_session_id", sessionId);
        node.put("req_email", "test@test.com");
        node.put("req_name", "TEST NAME");
        node.put("req_mobile_phone", "089696223555");
        node.put("req_basket", "testing");
        node.put("req_currency", "360");
        node.put("req_purchase_currency", "360");

        logger.info("node: " + node.toString());

        //RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

        HttpEntity<String> entity = new HttpEntity<String>("data=" + node.toString(), headers);

        // send request and parse result
        ResponseEntity<String> response = restTemplate.exchange(doku_va_url, HttpMethod.POST, entity, String.class);

        if (response.getStatusCode() == HttpStatus.OK) {
            String responseBody = response.getBody();
            logger.info(">> OK: " + responseBody);
            JsonNode root;
            try {
                root = mapper.readTree(responseBody);
                if ((root.get("res_response_code") != null) && "0000".equals(root.get("res_response_code").textValue() )) {
                    String payCode = (root.get("res_pay_code") != null) ? root.get("res_pay_code").textValue() : null;
                    String pairCode = (root.get("res_pairing_code") != null) ? root.get("res_pairing_code").textValue() : null;
                    logger.info("payCode: " + payCode);
                    logger.info("pairCode: " + pairCode);

                    message = "payIn succeed";
                    status = 1;
                    responseCode = HttpStatus.OK;
                    outputMap.put("payCode", payCode);
                    outputMap.put("pairCode", pairCode);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            logger.info(">> NOT OK: " + response.getStatusCode());
        }

        outputMap.put("status", status);
        outputMap.put("message", message);

        return new ResponseEntity<Object>(outputMap, responseCode);
    }

    private static String generateSHA1(String input){
        String sha1 = "";
        try {
            MessageDigest crypt = MessageDigest.getInstance("SHA-1");
            crypt.reset();
            crypt.update(input.getBytes("UTF-8"));
            sha1 = byteToHex(crypt.digest());
        } catch (NoSuchAlgorithmException e) {
            logger.error(e.getMessage());
        } catch (UnsupportedEncodingException e) {
            logger.error(e.getMessage());
        }
        logger.info("sha1: " + sha1);
        return sha1;
    }

    private static String byteToHex(final byte[] hash) {
        Formatter formatter = new Formatter();
        for (byte b : hash) {
            formatter.format("%02x", b);
        }
        String result = formatter.toString();
        formatter.close();
        return result;
    }

    private static String oneliner(String input) {
        return input.replaceAll("[\r\n]+", "");
    }

    // doku disburse ping
    @RequestMapping(value = "/doku/ping", method = RequestMethod.GET)
    public String ping(){
        String requestId = UUID.randomUUID().toString();
        String url = doku_disburse_url + "/ping";

        //RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        headers.set("agentKey", dokuAgentKey);
        headers.set("requestId", requestId);
        headers.set("signature", generateSignature(requestId));

        HttpEntity<String> entity = new HttpEntity<String>(headers);

        // send request and parse result
        ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.POST, entity, String.class);
        String output = response.getBody();
        logger.info("ping to: " + url);
        logger.info("ping response: " + oneliner(output));
        return output;
    }

    // generate doku AES signature
    private String generateSignature(String requestId) {
        // if (requestId == null) requestId = UUID.randomUUID().toString();
        String concatenatedString = dokuAgentKey + requestId;

        Key key = new SecretKeySpec(doku_encKey.getBytes(), ALGORITHM);
        Cipher c = null;
        String signature = null;
        try {
            c = Cipher.getInstance(ALGORITHM);
            c.init(Cipher.ENCRYPT_MODE, key);
            byte[] encodedValue = c.doFinal(concatenatedString.getBytes());
            signature = new String(Base64.encodeBase64(encodedValue));
        } catch (NoSuchAlgorithmException |
                NoSuchPaddingException |
                InvalidKeyException |
                BadPaddingException |
                IllegalBlockSizeException e) {
            logger.error("Error generating signature: " + e.getMessage());
        }
        // String urlEncoded = URLEncoder.encode(signature, "UTF-8");
        return signature;
    }

    // doku disbursement: inquiry + submit
    @RequestMapping(value = "/doku/disburse", method = RequestMethod.POST)
    public ResponseEntity<?> disburse(@RequestBody Map<String, String> inputMap) {
        String message = "disburse failed";
        int status = 1;
        HttpStatus responseCode = HttpStatus.INTERNAL_SERVER_ERROR;

        ObjectMapper objectMapper = new ObjectMapper();
        //RestTemplate restTemplate = new RestTemplate();

        // inquiry request
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        headers.set("agentKey", dokuAgentKey);
        String requestId = UUID.randomUUID().toString();
        headers.set("requestId", requestId);
        headers.set("signature", generateSignature(requestId));

        MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
        map.setAll(inputMap);
        HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(map, headers);

        // send request and parse result
        ResponseEntity<DokuInquiryResponse> response = restTemplate.postForEntity(doku_disburse_url + "/cashin/inquiry", request , DokuInquiryResponse.class);

        Map outputMap = new HashMap<>();
        String inquiryId = null;
        if (response.getStatusCode() == HttpStatus.OK) {
            DokuInquiryResponse dokuInquiryResponse = response.getBody();
            inquiryId = dokuInquiryResponse.getInquiry().getIdToken();
            logger.info("Inquiry ID: " + inquiryId);
            try {
                logger.info("Inquiry response body: " + oneliner(objectMapper.writeValueAsString(dokuInquiryResponse)));
            } catch (JsonProcessingException e) {
                logger.error("JsonProcessingException" + e.getMessage());
            }

            DokuDisbursementRequest disburseReq = generateDiDisbursementRequest(inputMap, inquiryId);

            String disburseReqString = null;
            try {
                disburseReqString = objectMapper.writeValueAsString(disburseReq);
            } catch (JsonProcessingException e) {
                logger.error("JsonProcessingException: " + e.getMessage());
            }

            logger.info("disburse request body: " + oneliner(disburseReqString));

            // inquiry request
            HttpHeaders headers2 = new HttpHeaders();
            headers2.setContentType(MediaType.APPLICATION_JSON);
            headers2.set("agentKey", dokuAgentKey);
            String requestId2 = UUID.randomUUID().toString();
            headers2.set("requestId", requestId2);
            headers2.set("signature", generateSignature(requestId2));

            HttpEntity<String> entity = new HttpEntity<>(disburseReqString, headers2);
            ResponseEntity<DokuDisbursementResponse> disburseResponseEntity = restTemplate.exchange(doku_disburse_url + "/cashin/remit", HttpMethod.POST, entity , DokuDisbursementResponse.class);

            DokuDisbursementResponse disburseResponse = disburseResponseEntity.getBody();
            String disburseResponseString = null;
            try {
                disburseResponseString = objectMapper.writeValueAsString(disburseResponse);
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }
            logger.info("disburse response body: " + oneliner(disburseResponseString));
            if (response.getStatusCode() == HttpStatus.OK) {
                status = 0;
                message = "disburse succeed";
                responseCode = HttpStatus.OK;
                outputMap.put("remit", disburseResponse.getRemit());
            }
        } else {
            logger.error("Inquiry response not OK: " + response.getBody());
            logger.error("Inquiry response body: " + response.getBody().getMessage());
        }
        outputMap.put("status", status);
        outputMap.put("message", message);

        return new ResponseEntity<Object>(outputMap, responseCode);
    }

    // populate disbursement request
    private DokuDisbursementRequest generateDiDisbursementRequest(Map<String, String> svm, String inquiryId) {
        DokuDisbursementRequest disburseReq = new DokuDisbursementRequest();
        disburseReq.setAuth1("1234");
        disburseReq.getBeneficiary().setAddress("");
        disburseReq.getBeneficiary().getCountry().setCode("ID");
        disburseReq.getBeneficiary().setFirstName("John");
        disburseReq.getBeneficiary().setLastName("Doe");
        disburseReq.getBeneficiary().setPhoneNumber("62816773344");

        disburseReq.getBeneficiaryAccount().setAddress(svm.get("beneficiaryAccount.address"));
        disburseReq.getBeneficiaryAccount().getBank().setCode(svm.get("beneficiaryAccount.bank.code"));
        disburseReq.getBeneficiaryAccount().getBank().setCountryCode(svm.get("beneficiaryAccount.bank.countryCode"));
        disburseReq.getBeneficiaryAccount().getBank().setId(svm.get("beneficiaryAccount.bank.id"));
        disburseReq.getBeneficiaryAccount().getBank().setName(svm.get("beneficiaryAccount.bank.name"));

        disburseReq.getBeneficiaryAccount().setCity(svm.get("beneficiaryAccount.city"));
        disburseReq.getBeneficiaryAccount().setName(svm.get("beneficiaryAccount.name"));
        disburseReq.getBeneficiaryAccount().setNumber(svm.get("beneficiaryAccount.number"));

        disburseReq.setBeneficiaryCity("");
        disburseReq.getBeneficiaryCountry().setCode(svm.get("beneficiaryCountry.Code"));
        disburseReq.getBeneficiaryCurrency().setCode(svm.get("beneficiaryCurrency.Code"));
        disburseReq.setBeneficiaryWalletId("");

        disburseReq.getChannel().setCode("07");
        disburseReq.getInquiry().setIdToken(inquiryId);

        disburseReq.getSender().setAddress("somewhere");
        disburseReq.getSender().setBirthDate("1988-05-25");
        disburseReq.getSender().getCountry().setCode(svm.get("senderCountry.Code"));
        disburseReq.getSender().setFirstName("Jane");
        disburseReq.getSender().setLastName("Doe");
        disburseReq.getSender().setGender("FEMALE");
        disburseReq.getSender().setPersonalId("4509932907");
        disburseReq.getSender().getPersonalIdCountry().setCode("ID");
        disburseReq.getSender().setPersonalIdExpireDate("2020-01-01");
        disburseReq.getSender().setPersonalIdIssueDate("2008-07-18");
        disburseReq.getSender().setPersonalIdType("PASSPORT");
        disburseReq.getSender().setPhoneNumber("7-916-5786212");

        disburseReq.setSenderAmount(Integer.valueOf(svm.get("senderAmount")));
        disburseReq.getSenderCountry().setCode(svm.get("senderCountry.Code"));
        disburseReq.getSenderCurrency().setCode(svm.get("senderCurrency.Code"));
        disburseReq.setSenderNote("test staging");
        disburseReq.setSendType("senderAmount");

        return disburseReq;
    }

    @RequestMapping(value = "/doku/transactionStatus/{txnId}", method = RequestMethod.GET)
    public String transactionStatus(@PathVariable("txnId") String txnId) {

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        String requestId = UUID.randomUUID().toString();

        MultiValueMap<String, String> bodyMap = new LinkedMultiValueMap<>();
        bodyMap.add("agentKey", dokuAgentKey);
        bodyMap.add("requestId", requestId);
        bodyMap.add("signature", generateSignature(requestId));
        bodyMap.add("transactionId", txnId);

        HttpEntity<MultiValueMap<String, String>> entity = new HttpEntity<>(bodyMap, headers);

        ResponseEntity<String> response = restTemplate.exchange(doku_disburse_url + "/transaction/info", HttpMethod.POST, entity , String.class);
        String responseBody = response.getBody();
        logger.info("transaction status: " + oneliner(responseBody));
        return responseBody;
    }

}