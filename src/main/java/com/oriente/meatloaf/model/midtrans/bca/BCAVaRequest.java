package com.oriente.meatloaf.model.midtrans.bca;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.validation.Valid;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "payment_type",
        "transaction_details",
        "customer_details",
        "item_details",
        "bank_transfer"
})
public class BCAVaRequest {

    @JsonProperty("payment_type")
    private String paymentType;
    @JsonProperty("transaction_details")
    @Valid
    private TransactionDetails transactionDetails;
    @JsonProperty("customer_details")
    @Valid
    private CustomerDetails customerDetails;
    @JsonProperty("item_details")
    @Valid
    private List<ItemDetail> itemDetails = null;
    @JsonProperty("bank_transfer")
    @Valid
    private BankTransfer bankTransfer;
    @JsonIgnore
    @Valid
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * No args constructor for use in serialization
     *
     */
    public BCAVaRequest() {
    }

    /**
     *
     * @param bankTransfer
     * @param paymentType
     * @param customerDetails
     * @param transactionDetails
     * @param itemDetails
     */
    public BCAVaRequest(String paymentType, TransactionDetails transactionDetails, CustomerDetails customerDetails, List<ItemDetail> itemDetails, BankTransfer bankTransfer) {
        super();
        this.paymentType = paymentType;
        this.transactionDetails = transactionDetails;
        this.customerDetails = customerDetails;
        this.itemDetails = itemDetails;
        this.bankTransfer = bankTransfer;
    }

    @JsonProperty("payment_type")
    public String getPaymentType() {
        return paymentType;
    }

    @JsonProperty("payment_type")
    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public BCAVaRequest withPaymentType(String paymentType) {
        this.paymentType = paymentType;
        return this;
    }

    @JsonProperty("transaction_details")
    public TransactionDetails getTransactionDetails() {
        return transactionDetails;
    }

    @JsonProperty("transaction_details")
    public void setTransactionDetails(TransactionDetails transactionDetails) {
        this.transactionDetails = transactionDetails;
    }

    public BCAVaRequest withTransactionDetails(TransactionDetails transactionDetails) {
        this.transactionDetails = transactionDetails;
        return this;
    }

    @JsonProperty("customer_details")
    public CustomerDetails getCustomerDetails() {
        return customerDetails;
    }

    @JsonProperty("customer_details")
    public void setCustomerDetails(CustomerDetails customerDetails) {
        this.customerDetails = customerDetails;
    }

    public BCAVaRequest withCustomerDetails(CustomerDetails customerDetails) {
        this.customerDetails = customerDetails;
        return this;
    }

    @JsonProperty("item_details")
    public List<ItemDetail> getItemDetails() {
        return itemDetails;
    }

    @JsonProperty("item_details")
    public void setItemDetails(List<ItemDetail> itemDetails) {
        this.itemDetails = itemDetails;
    }

    public BCAVaRequest withItemDetails(List<ItemDetail> itemDetails) {
        this.itemDetails = itemDetails;
        return this;
    }

    @JsonProperty("bank_transfer")
    public BankTransfer getBankTransfer() {
        return bankTransfer;
    }

    @JsonProperty("bank_transfer")
    public void setBankTransfer(BankTransfer bankTransfer) {
        this.bankTransfer = bankTransfer;
    }

    public BCAVaRequest withBankTransfer(BankTransfer bankTransfer) {
        this.bankTransfer = bankTransfer;
        return this;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public BCAVaRequest withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("paymentType", paymentType).append("transactionDetails", transactionDetails).append("customerDetails", customerDetails).append("itemDetails", itemDetails).append("bankTransfer", bankTransfer).append("additionalProperties", additionalProperties).toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(additionalProperties).append(bankTransfer).append(paymentType).append(customerDetails).append(transactionDetails).append(itemDetails).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof BCAVaRequest) == false) {
            return false;
        }
        BCAVaRequest rhs = ((BCAVaRequest) other);
        return new EqualsBuilder().append(additionalProperties, rhs.additionalProperties).append(bankTransfer, rhs.bankTransfer).append(paymentType, rhs.paymentType).append(customerDetails, rhs.customerDetails).append(transactionDetails, rhs.transactionDetails).append(itemDetails, rhs.itemDetails).isEquals();
    }

}