-- MySQL dump 10.16  Distrib 10.2.13-MariaDB, for osx10.12 (x86_64)
--
-- Host: localhost    Database: meatloaf
-- ------------------------------------------------------
-- Server version	10.2.11-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `cwm_payments`
--

DROP TABLE IF EXISTS `cwm_payments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cwm_payments` (
  `id` varchar(255) NOT NULL,
  `account_no` varchar(255) DEFAULT NULL,
  `channel` varchar(255) DEFAULT NULL,
  `date_added` datetime DEFAULT NULL,
  `date_updated` datetime DEFAULT NULL,
  `processor` varchar(255) DEFAULT NULL,
  `raw_message` varchar(255) DEFAULT NULL,
  `remarks` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `txn_amount` float DEFAULT NULL,
  `txn_id` varchar(255) DEFAULT NULL,
  `txn_ref_no` varchar(255) DEFAULT NULL,
  `txn_type` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cwm_payments`
--

LOCK TABLES `cwm_payments` WRITE;
/*!40000 ALTER TABLE `cwm_payments` DISABLE KEYS */;
INSERT INTO `cwm_payments` VALUES ('1',NULL,NULL,NULL,NULL,NULL,'{\"status_code\": \"201\", \"transaction_id\": \"a9aed5972-5b6a-401e-894b-a32c91ed1a3a\", \"transaction_status\": \"pending\", \"fraud_status\": \"accept\", \"status_message\": \"Veritrans payment notification\"}',NULL,NULL,0,NULL,NULL,0);
/*!40000 ALTER TABLE `cwm_payments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `payment`
--

DROP TABLE IF EXISTS `payment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `payment` (
  `internal_id` varchar(255) NOT NULL,
  `amount` decimal(19,2) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `external_id` varchar(255) DEFAULT NULL,
  `external_type` int(11) DEFAULT NULL,
  `loan_item_id` varchar(255) DEFAULT NULL,
  `modify_date` datetime DEFAULT NULL,
  `note` varchar(255) DEFAULT NULL,
  `state` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`internal_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `payment`
--

LOCK TABLES `payment` WRITE;
/*!40000 ALTER TABLE `payment` DISABLE KEYS */;
/*!40000 ALTER TABLE `payment` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-03-09 11:22:50
