
package com.oriente.meatloaf.model.doku.inquiry;

import java.util.HashMap;
import java.util.Map;
import javax.validation.Valid;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "id",
    "forex",
    "rate",
    "createdTime"
})
public class ForexReference {

    @JsonProperty("id")
    private Integer id;
    @JsonProperty("forex")
    @Valid
    private Forex forex;
    @JsonProperty("rate")
    private Double rate;
    @JsonProperty("createdTime")
    private Long createdTime;
    @JsonIgnore
    @Valid
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * No args constructor for use in serialization
     * 
     */
    public ForexReference() {
    }

    /**
     * 
     * @param id
     * @param forex
     * @param rate
     * @param createdTime
     */
    public ForexReference(Integer id, Forex forex, Double rate, Long createdTime) {
        super();
        this.id = id;
        this.forex = forex;
        this.rate = rate;
        this.createdTime = createdTime;
    }

    @JsonProperty("id")
    public Integer getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(Integer id) {
        this.id = id;
    }

    public ForexReference withId(Integer id) {
        this.id = id;
        return this;
    }

    @JsonProperty("forex")
    public Forex getForex() {
        return forex;
    }

    @JsonProperty("forex")
    public void setForex(Forex forex) {
        this.forex = forex;
    }

    public ForexReference withForex(Forex forex) {
        this.forex = forex;
        return this;
    }

    @JsonProperty("rate")
    public Double getRate() {
        return rate;
    }

    @JsonProperty("rate")
    public void setRate(Double rate) {
        this.rate = rate;
    }

    public ForexReference withRate(Double rate) {
        this.rate = rate;
        return this;
    }

    @JsonProperty("createdTime")
    public Long getCreatedTime() {
        return createdTime;
    }

    @JsonProperty("createdTime")
    public void setCreatedTime(Long createdTime) {
        this.createdTime = createdTime;
    }

    public ForexReference withCreatedTime(Long createdTime) {
        this.createdTime = createdTime;
        return this;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public ForexReference withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("id", id).append("forex", forex).append("rate", rate).append("createdTime", createdTime).append("additionalProperties", additionalProperties).toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(id).append(forex).append(rate).append(createdTime).append(additionalProperties).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof ForexReference) == false) {
            return false;
        }
        ForexReference rhs = ((ForexReference) other);
        return new EqualsBuilder().append(id, rhs.id).append(forex, rhs.forex).append(rate, rhs.rate).append(createdTime, rhs.createdTime).append(additionalProperties, rhs.additionalProperties).isEquals();
    }

}
