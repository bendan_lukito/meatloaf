package com.oriente.meatloaf.model.midtrans.permata;

import java.util.HashMap;
import java.util.Map;
import javax.validation.Valid;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "payment_type",
        "bank_transfer",
        "transaction_details"
})
public class PermataVaRequest {

    @JsonProperty("payment_type")
    private String paymentType;
    @JsonProperty("bank_transfer")
    @Valid
    private BankTransfer bankTransfer;
    @JsonProperty("transaction_details")
    @Valid
    private TransactionDetails transactionDetails;
    @JsonIgnore
    @Valid
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * No args constructor for use in serialization
     *
     */
    public PermataVaRequest() {
    }

    /**
     *
     * @param bankTransfer
     * @param paymentType
     * @param transactionDetails
     */
    public PermataVaRequest(String paymentType, BankTransfer bankTransfer, TransactionDetails transactionDetails) {
        super();
        this.paymentType = paymentType;
        this.bankTransfer = bankTransfer;
        this.transactionDetails = transactionDetails;
    }

    @JsonProperty("payment_type")
    public String getPaymentType() {
        return paymentType;
    }

    @JsonProperty("payment_type")
    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public PermataVaRequest withPaymentType(String paymentType) {
        this.paymentType = paymentType;
        return this;
    }

    @JsonProperty("bank_transfer")
    public BankTransfer getBankTransfer() {
        return bankTransfer;
    }

    @JsonProperty("bank_transfer")
    public void setBankTransfer(BankTransfer bankTransfer) {
        this.bankTransfer = bankTransfer;
    }

    public PermataVaRequest withBankTransfer(BankTransfer bankTransfer) {
        this.bankTransfer = bankTransfer;
        return this;
    }

    @JsonProperty("transaction_details")
    public TransactionDetails getTransactionDetails() {
        return transactionDetails;
    }

    @JsonProperty("transaction_details")
    public void setTransactionDetails(TransactionDetails transactionDetails) {
        this.transactionDetails = transactionDetails;
    }

    public PermataVaRequest withTransactionDetails(TransactionDetails transactionDetails) {
        this.transactionDetails = transactionDetails;
        return this;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public PermataVaRequest withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("paymentType", paymentType).append("bankTransfer", bankTransfer).append("transactionDetails", transactionDetails).append("additionalProperties", additionalProperties).toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(additionalProperties).append(bankTransfer).append(paymentType).append(transactionDetails).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof PermataVaRequest) == false) {
            return false;
        }
        PermataVaRequest rhs = ((PermataVaRequest) other);
        return new EqualsBuilder().append(additionalProperties, rhs.additionalProperties).append(bankTransfer, rhs.bankTransfer).append(paymentType, rhs.paymentType).append(transactionDetails, rhs.transactionDetails).isEquals();
    }

}