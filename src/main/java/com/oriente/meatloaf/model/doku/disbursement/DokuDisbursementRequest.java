
package com.oriente.meatloaf.model.doku.disbursement;

import java.util.HashMap;
import java.util.Map;
import javax.validation.Valid;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "sendType",
    "auth1",
    "beneficiary",
    "beneficiaryAccount",
    "beneficiaryCity",
    "beneficiaryCountry",
    "beneficiaryCurrency",
    "channel",
    "inquiry",
    "sender",
    "senderAmount",
    "senderCountry",
    "senderCurrency",
    "senderNote",
    "beneficiaryWalletId"
})
public class DokuDisbursementRequest {

    @JsonProperty("sendType")
    private String sendType;
    @JsonProperty("auth1")
    private String auth1;
    @JsonProperty("beneficiary")
    @Valid
    private Beneficiary beneficiary;
    @JsonProperty("beneficiaryAccount")
    @Valid
    private BeneficiaryAccount beneficiaryAccount;
    @JsonProperty("beneficiaryCity")
    private String beneficiaryCity;
    @JsonProperty("beneficiaryCountry")
    @Valid
    private BeneficiaryCountry beneficiaryCountry;
    @JsonProperty("beneficiaryCurrency")
    @Valid
    private BeneficiaryCurrency beneficiaryCurrency;
    @JsonProperty("channel")
    @Valid
    private Channel channel;
    @JsonProperty("inquiry")
    @Valid
    private Inquiry inquiry;
    @JsonProperty("sender")
    @Valid
    private Sender sender;
    @JsonProperty("senderAmount")
    private Integer senderAmount;
    @JsonProperty("senderCountry")
    @Valid
    private SenderCountry senderCountry;
    @JsonProperty("senderCurrency")
    @Valid
    private SenderCurrency senderCurrency;
    @JsonProperty("senderNote")
    private String senderNote;
    @JsonProperty("beneficiaryWalletId")
    private String beneficiaryWalletId;
    @JsonIgnore
    @Valid
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * No args constructor for use in serialization
     * 
     */
    public DokuDisbursementRequest() {
        Beneficiary beneficiary = new Beneficiary();
        beneficiary.setCountry(new Country());
        this.setBeneficiary(beneficiary);

        BeneficiaryAccount beneficiaryAccount = new BeneficiaryAccount();
        beneficiaryAccount.setBank(new Bank());
        this.setBeneficiaryAccount(beneficiaryAccount);

        this.setBeneficiaryCountry(new BeneficiaryCountry());
        this.setBeneficiaryCurrency(new BeneficiaryCurrency());
        this.setChannel(new Channel());
        this.setInquiry(new Inquiry());

        Sender sender = new Sender();
        sender.setCountry(new Country_());
        sender.setPersonalIdCountry(new PersonalIdCountry());
        this.setSender(sender);

        this.setSenderCountry(new SenderCountry());
        this.setSenderCurrency(new SenderCurrency());
    }

    /**
     * 
     * @param auth1
     * @param beneficiaryAccount
     * @param sendType
     * @param senderNote
     * @param beneficiary
     * @param beneficiaryCountry
     * @param senderCurrency
     * @param beneficiaryWalletId
     * @param sender
     * @param senderCountry
     * @param inquiry
     * @param beneficiaryCity
     * @param senderAmount
     * @param channel
     * @param beneficiaryCurrency
     */
    public DokuDisbursementRequest(String sendType, String auth1, Beneficiary beneficiary, BeneficiaryAccount beneficiaryAccount, String beneficiaryCity, BeneficiaryCountry beneficiaryCountry, BeneficiaryCurrency beneficiaryCurrency, Channel channel, Inquiry inquiry, Sender sender, Integer senderAmount, SenderCountry senderCountry, SenderCurrency senderCurrency, String senderNote, String beneficiaryWalletId) {
        super();
        this.sendType = sendType;
        this.auth1 = auth1;
        this.beneficiary = beneficiary;
        this.beneficiaryAccount = beneficiaryAccount;
        this.beneficiaryCity = beneficiaryCity;
        this.beneficiaryCountry = beneficiaryCountry;
        this.beneficiaryCurrency = beneficiaryCurrency;
        this.channel = channel;
        this.inquiry = inquiry;
        this.sender = sender;
        this.senderAmount = senderAmount;
        this.senderCountry = senderCountry;
        this.senderCurrency = senderCurrency;
        this.senderNote = senderNote;
        this.beneficiaryWalletId = beneficiaryWalletId;
    }

    @JsonProperty("sendType")
    public String getSendType() {
        return sendType;
    }

    @JsonProperty("sendType")
    public void setSendType(String sendType) {
        this.sendType = sendType;
    }

    public DokuDisbursementRequest withSendType(String sendType) {
        this.sendType = sendType;
        return this;
    }

    @JsonProperty("auth1")
    public String getAuth1() {
        return auth1;
    }

    @JsonProperty("auth1")
    public void setAuth1(String auth1) {
        this.auth1 = auth1;
    }

    public DokuDisbursementRequest withAuth1(String auth1) {
        this.auth1 = auth1;
        return this;
    }

    @JsonProperty("beneficiary")
    public Beneficiary getBeneficiary() {
        return beneficiary;
    }

    @JsonProperty("beneficiary")
    public void setBeneficiary(Beneficiary beneficiary) {
        this.beneficiary = beneficiary;
    }

    public DokuDisbursementRequest withBeneficiary(Beneficiary beneficiary) {
        this.beneficiary = beneficiary;
        return this;
    }

    @JsonProperty("beneficiaryAccount")
    public BeneficiaryAccount getBeneficiaryAccount() {
        return beneficiaryAccount;
    }

    @JsonProperty("beneficiaryAccount")
    public void setBeneficiaryAccount(BeneficiaryAccount beneficiaryAccount) {
        this.beneficiaryAccount = beneficiaryAccount;
    }

    public DokuDisbursementRequest withBeneficiaryAccount(BeneficiaryAccount beneficiaryAccount) {
        this.beneficiaryAccount = beneficiaryAccount;
        return this;
    }

    @JsonProperty("beneficiaryCity")
    public String getBeneficiaryCity() {
        return beneficiaryCity;
    }

    @JsonProperty("beneficiaryCity")
    public void setBeneficiaryCity(String beneficiaryCity) {
        this.beneficiaryCity = beneficiaryCity;
    }

    public DokuDisbursementRequest withBeneficiaryCity(String beneficiaryCity) {
        this.beneficiaryCity = beneficiaryCity;
        return this;
    }

    @JsonProperty("beneficiaryCountry")
    public BeneficiaryCountry getBeneficiaryCountry() {
        return beneficiaryCountry;
    }

    @JsonProperty("beneficiaryCountry")
    public void setBeneficiaryCountry(BeneficiaryCountry beneficiaryCountry) {
        this.beneficiaryCountry = beneficiaryCountry;
    }

    public DokuDisbursementRequest withBeneficiaryCountry(BeneficiaryCountry beneficiaryCountry) {
        this.beneficiaryCountry = beneficiaryCountry;
        return this;
    }

    @JsonProperty("beneficiaryCurrency")
    public BeneficiaryCurrency getBeneficiaryCurrency() {
        return beneficiaryCurrency;
    }

    @JsonProperty("beneficiaryCurrency")
    public void setBeneficiaryCurrency(BeneficiaryCurrency beneficiaryCurrency) {
        this.beneficiaryCurrency = beneficiaryCurrency;
    }

    public DokuDisbursementRequest withBeneficiaryCurrency(BeneficiaryCurrency beneficiaryCurrency) {
        this.beneficiaryCurrency = beneficiaryCurrency;
        return this;
    }

    @JsonProperty("channel")
    public Channel getChannel() {
        return channel;
    }

    @JsonProperty("channel")
    public void setChannel(Channel channel) {
        this.channel = channel;
    }

    public DokuDisbursementRequest withChannel(Channel channel) {
        this.channel = channel;
        return this;
    }

    @JsonProperty("inquiry")
    public Inquiry getInquiry() {
        return inquiry;
    }

    @JsonProperty("inquiry")
    public void setInquiry(Inquiry inquiry) {
        this.inquiry = inquiry;
    }

    public DokuDisbursementRequest withInquiry(Inquiry inquiry) {
        this.inquiry = inquiry;
        return this;
    }

    @JsonProperty("sender")
    public Sender getSender() {
        return sender;
    }

    @JsonProperty("sender")
    public void setSender(Sender sender) {
        this.sender = sender;
    }

    public DokuDisbursementRequest withSender(Sender sender) {
        this.sender = sender;
        return this;
    }

    @JsonProperty("senderAmount")
    public Integer getSenderAmount() {
        return senderAmount;
    }

    @JsonProperty("senderAmount")
    public void setSenderAmount(Integer senderAmount) {
        this.senderAmount = senderAmount;
    }

    public DokuDisbursementRequest withSenderAmount(Integer senderAmount) {
        this.senderAmount = senderAmount;
        return this;
    }

    @JsonProperty("senderCountry")
    public SenderCountry getSenderCountry() {
        return senderCountry;
    }

    @JsonProperty("senderCountry")
    public void setSenderCountry(SenderCountry senderCountry) {
        this.senderCountry = senderCountry;
    }

    public DokuDisbursementRequest withSenderCountry(SenderCountry senderCountry) {
        this.senderCountry = senderCountry;
        return this;
    }

    @JsonProperty("senderCurrency")
    public SenderCurrency getSenderCurrency() {
        return senderCurrency;
    }

    @JsonProperty("senderCurrency")
    public void setSenderCurrency(SenderCurrency senderCurrency) {
        this.senderCurrency = senderCurrency;
    }

    public DokuDisbursementRequest withSenderCurrency(SenderCurrency senderCurrency) {
        this.senderCurrency = senderCurrency;
        return this;
    }

    @JsonProperty("senderNote")
    public String getSenderNote() {
        return senderNote;
    }

    @JsonProperty("senderNote")
    public void setSenderNote(String senderNote) {
        this.senderNote = senderNote;
    }

    public DokuDisbursementRequest withSenderNote(String senderNote) {
        this.senderNote = senderNote;
        return this;
    }

    @JsonProperty("beneficiaryWalletId")
    public String getBeneficiaryWalletId() {
        return beneficiaryWalletId;
    }

    @JsonProperty("beneficiaryWalletId")
    public void setBeneficiaryWalletId(String beneficiaryWalletId) {
        this.beneficiaryWalletId = beneficiaryWalletId;
    }

    public DokuDisbursementRequest withBeneficiaryWalletId(String beneficiaryWalletId) {
        this.beneficiaryWalletId = beneficiaryWalletId;
        return this;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public DokuDisbursementRequest withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("sendType", sendType).append("auth1", auth1).append("beneficiary", beneficiary).append("beneficiaryAccount", beneficiaryAccount).append("beneficiaryCity", beneficiaryCity).append("beneficiaryCountry", beneficiaryCountry).append("beneficiaryCurrency", beneficiaryCurrency).append("channel", channel).append("inquiry", inquiry).append("sender", sender).append("senderAmount", senderAmount).append("senderCountry", senderCountry).append("senderCurrency", senderCurrency).append("senderNote", senderNote).append("beneficiaryWalletId", beneficiaryWalletId).append("additionalProperties", additionalProperties).toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(beneficiaryAccount).append(auth1).append(sendType).append(senderNote).append(beneficiaryCountry).append(beneficiary).append(senderCurrency).append(beneficiaryWalletId).append(sender).append(senderCountry).append(inquiry).append(additionalProperties).append(beneficiaryCity).append(senderAmount).append(channel).append(beneficiaryCurrency).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof DokuDisbursementRequest) == false) {
            return false;
        }
        DokuDisbursementRequest rhs = ((DokuDisbursementRequest) other);
        return new EqualsBuilder().append(beneficiaryAccount, rhs.beneficiaryAccount).append(auth1, rhs.auth1).append(sendType, rhs.sendType).append(senderNote, rhs.senderNote).append(beneficiaryCountry, rhs.beneficiaryCountry).append(beneficiary, rhs.beneficiary).append(senderCurrency, rhs.senderCurrency).append(beneficiaryWalletId, rhs.beneficiaryWalletId).append(sender, rhs.sender).append(senderCountry, rhs.senderCountry).append(inquiry, rhs.inquiry).append(additionalProperties, rhs.additionalProperties).append(beneficiaryCity, rhs.beneficiaryCity).append(senderAmount, rhs.senderAmount).append(channel, rhs.channel).append(beneficiaryCurrency, rhs.beneficiaryCurrency).isEquals();
    }

}
