package com.oriente.meatloaf.model.midtrans.permata;

import java.util.HashMap;
import java.util.Map;
import javax.validation.Valid;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "bank",
        "permata"
})
public class BankTransfer {

    @JsonProperty("bank")
    private String bank;
    @JsonProperty("permata")
    @Valid
    private Permata permata;
    @JsonIgnore
    @Valid
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * No args constructor for use in serialization
     *
     */
    public BankTransfer() {
    }

    /**
     *
     * @param permata
     * @param bank
     */
    public BankTransfer(String bank, Permata permata) {
        super();
        this.bank = bank;
        this.permata = permata;
    }

    @JsonProperty("bank")
    public String getBank() {
        return bank;
    }

    @JsonProperty("bank")
    public void setBank(String bank) {
        this.bank = bank;
    }

    public BankTransfer withBank(String bank) {
        this.bank = bank;
        return this;
    }

    @JsonProperty("permata")
    public Permata getPermata() {
        return permata;
    }

    @JsonProperty("permata")
    public void setPermata(Permata permata) {
        this.permata = permata;
    }

    public BankTransfer withPermata(Permata permata) {
        this.permata = permata;
        return this;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public BankTransfer withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("bank", bank).append("permata", permata).append("additionalProperties", additionalProperties).toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(permata).append(additionalProperties).append(bank).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof BankTransfer) == false) {
            return false;
        }
        BankTransfer rhs = ((BankTransfer) other);
        return new EqualsBuilder().append(permata, rhs.permata).append(additionalProperties, rhs.additionalProperties).append(bank, rhs.bank).isEquals();
    }

}