package com.oriente.meatloaf.model.midtrans.permata;

import java.util.HashMap;
import java.util.Map;
import javax.validation.Valid;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "status_code",
        "status_message",
        "transaction_id",
        "order_id",
        "gross_amount",
        "payment_type",
        "transaction_time",
        "transaction_status",
        "fraud_status",
        "permata_va_number",
        "signature_key"
})
public class PermataVaResponse {

    @JsonProperty("status_code")
    private String statusCode;
    @JsonProperty("status_message")
    private String statusMessage;
    @JsonProperty("transaction_id")
    private String transactionId;
    @JsonProperty("order_id")
    private String orderId;
    @JsonProperty("gross_amount")
    private String grossAmount;
    @JsonProperty("payment_type")
    private String paymentType;
    @JsonProperty("transaction_time")
    private String transactionTime;
    @JsonProperty("transaction_status")
    private String transactionStatus;
    @JsonProperty("fraud_status")
    private String fraudStatus;
    @JsonProperty("permata_va_number")
    private String permataVaNumber;
    @JsonProperty("signature_key")
    private String signatureKey;
    @JsonIgnore
    @Valid
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("status_code")
    public String getStatusCode() {
        return statusCode;
    }

    @JsonProperty("status_code")
    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    @JsonProperty("status_message")
    public String getStatusMessage() {
        return statusMessage;
    }

    @JsonProperty("status_message")
    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

    @JsonProperty("transaction_id")
    public String getTransactionId() {
        return transactionId;
    }

    @JsonProperty("transaction_id")
    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    @JsonProperty("order_id")
    public String getOrderId() {
        return orderId;
    }

    @JsonProperty("order_id")
    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    @JsonProperty("gross_amount")
    public String getGrossAmount() {
        return grossAmount;
    }

    @JsonProperty("gross_amount")
    public void setGrossAmount(String grossAmount) {
        this.grossAmount = grossAmount;
    }

    @JsonProperty("payment_type")
    public String getPaymentType() {
        return paymentType;
    }

    @JsonProperty("payment_type")
    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    @JsonProperty("transaction_time")
    public String getTransactionTime() {
        return transactionTime;
    }

    @JsonProperty("transaction_time")
    public void setTransactionTime(String transactionTime) {
        this.transactionTime = transactionTime;
    }

    @JsonProperty("transaction_status")
    public String getTransactionStatus() {
        return transactionStatus;
    }

    @JsonProperty("transaction_status")
    public void setTransactionStatus(String transactionStatus) {
        this.transactionStatus = transactionStatus;
    }

    @JsonProperty("fraud_status")
    public String getFraudStatus() {
        return fraudStatus;
    }

    @JsonProperty("fraud_status")
    public void setFraudStatus(String fraudStatus) {
        this.fraudStatus = fraudStatus;
    }

    @JsonProperty("permata_va_number")
    public String getPermataVaNumber() {
        return permataVaNumber;
    }

    @JsonProperty("permata_va_number")
    public void setPermataVaNumber(String permataVaNumber) {
        this.permataVaNumber = permataVaNumber;
    }

    @JsonProperty("signature_key")
    public String getSignatureKey() {
        return signatureKey;
    }

    @JsonProperty("signature_key")
    public void setSignatureKey(String signatureKey) {
        this.signatureKey = signatureKey;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("statusCode", statusCode).append("statusMessage", statusMessage).append("transactionId", transactionId).append("orderId", orderId).append("grossAmount", grossAmount).append("paymentType", paymentType).append("transactionTime", transactionTime).append("transactionStatus", transactionStatus).append("fraudStatus", fraudStatus).append("permataVaNumber", permataVaNumber).append("signatureKey", signatureKey).append("additionalProperties", additionalProperties).toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(transactionId).append(paymentType).append(signatureKey).append(grossAmount).append(fraudStatus).append(statusMessage).append(statusCode).append(transactionTime).append(additionalProperties).append(transactionStatus).append(orderId).append(permataVaNumber).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof PermataVaResponse) == false) {
            return false;
        }
        PermataVaResponse rhs = ((PermataVaResponse) other);
        return new EqualsBuilder().append(transactionId, rhs.transactionId).append(paymentType, rhs.paymentType).append(signatureKey, rhs.signatureKey).append(grossAmount, rhs.grossAmount).append(fraudStatus, rhs.fraudStatus).append(statusMessage, rhs.statusMessage).append(statusCode, rhs.statusCode).append(transactionTime, rhs.transactionTime).append(additionalProperties, rhs.additionalProperties).append(transactionStatus, rhs.transactionStatus).append(orderId, rhs.orderId).append(permataVaNumber, rhs.permataVaNumber).isEquals();
    }

}
