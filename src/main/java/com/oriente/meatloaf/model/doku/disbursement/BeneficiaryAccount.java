
package com.oriente.meatloaf.model.doku.disbursement;

import java.util.HashMap;
import java.util.Map;
import javax.validation.Valid;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "address",
    "bank",
    "city",
    "name",
    "number"
})
public class BeneficiaryAccount {

    @JsonProperty("address")
    private String address;
    @JsonProperty("bank")
    @Valid
    private Bank bank;
    @JsonProperty("city")
    private String city;
    @JsonProperty("name")
    private String name;
    @JsonProperty("number")
    private String number;
    @JsonIgnore
    @Valid
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * No args constructor for use in serialization
     * 
     */
    public BeneficiaryAccount() {
    }

    /**
     * 
     * @param address
     * @param name
     * @param number
     * @param bank
     * @param city
     */
    public BeneficiaryAccount(String address, Bank bank, String city, String name, String number) {
        super();
        this.address = address;
        this.bank = bank;
        this.city = city;
        this.name = name;
        this.number = number;
    }

    @JsonProperty("address")
    public String getAddress() {
        return address;
    }

    @JsonProperty("address")
    public void setAddress(String address) {
        this.address = address;
    }

    public BeneficiaryAccount withAddress(String address) {
        this.address = address;
        return this;
    }

    @JsonProperty("bank")
    public Bank getBank() {
        return bank;
    }

    @JsonProperty("bank")
    public void setBank(Bank bank) {
        this.bank = bank;
    }

    public BeneficiaryAccount withBank(Bank bank) {
        this.bank = bank;
        return this;
    }

    @JsonProperty("city")
    public String getCity() {
        return city;
    }

    @JsonProperty("city")
    public void setCity(String city) {
        this.city = city;
    }

    public BeneficiaryAccount withCity(String city) {
        this.city = city;
        return this;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    public BeneficiaryAccount withName(String name) {
        this.name = name;
        return this;
    }

    @JsonProperty("number")
    public String getNumber() {
        return number;
    }

    @JsonProperty("number")
    public void setNumber(String number) {
        this.number = number;
    }

    public BeneficiaryAccount withNumber(String number) {
        this.number = number;
        return this;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public BeneficiaryAccount withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("address", address).append("bank", bank).append("city", city).append("name", name).append("number", number).append("additionalProperties", additionalProperties).toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(address).append(additionalProperties).append(name).append(number).append(bank).append(city).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof BeneficiaryAccount) == false) {
            return false;
        }
        BeneficiaryAccount rhs = ((BeneficiaryAccount) other);
        return new EqualsBuilder().append(address, rhs.address).append(additionalProperties, rhs.additionalProperties).append(name, rhs.name).append(number, rhs.number).append(bank, rhs.bank).append(city, rhs.city).isEquals();
    }

}
