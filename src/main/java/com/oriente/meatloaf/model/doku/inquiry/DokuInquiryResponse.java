
package com.oriente.meatloaf.model.doku.inquiry;

import java.util.HashMap;
import java.util.Map;
import javax.validation.Valid;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "status",
    "message",
    "inquiry"
})
public class DokuInquiryResponse {

    @JsonProperty("status")
    private Integer status;
    @JsonProperty("message")
    private String message;
    @JsonProperty("inquiry")
    @Valid
    private Inquiry inquiry;
    @JsonIgnore
    @Valid
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * No args constructor for use in serialization
     * 
     */
    public DokuInquiryResponse() {
    }

    /**
     * 
     * @param message
     * @param inquiry
     * @param status
     */
    public DokuInquiryResponse(Integer status, String message, Inquiry inquiry) {
        super();
        this.status = status;
        this.message = message;
        this.inquiry = inquiry;
    }

    @JsonProperty("status")
    public Integer getStatus() {
        return status;
    }

    @JsonProperty("status")
    public void setStatus(Integer status) {
        this.status = status;
    }

    public DokuInquiryResponse withStatus(Integer status) {
        this.status = status;
        return this;
    }

    @JsonProperty("message")
    public String getMessage() {
        return message;
    }

    @JsonProperty("message")
    public void setMessage(String message) {
        this.message = message;
    }

    public DokuInquiryResponse withMessage(String message) {
        this.message = message;
        return this;
    }

    @JsonProperty("inquiry")
    public Inquiry getInquiry() {
        return inquiry;
    }

    @JsonProperty("inquiry")
    public void setInquiry(Inquiry inquiry) {
        this.inquiry = inquiry;
    }

    public DokuInquiryResponse withInquiry(Inquiry inquiry) {
        this.inquiry = inquiry;
        return this;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public DokuInquiryResponse withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("status", status).append("message", message).append("inquiry", inquiry).append("additionalProperties", additionalProperties).toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(message).append(inquiry).append(status).append(additionalProperties).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof DokuInquiryResponse) == false) {
            return false;
        }
        DokuInquiryResponse rhs = ((DokuInquiryResponse) other);
        return new EqualsBuilder().append(message, rhs.message).append(inquiry, rhs.inquiry).append(status, rhs.status).append(additionalProperties, rhs.additionalProperties).isEquals();
    }

}
