
package com.oriente.meatloaf.model.doku.inquiry;

import java.util.HashMap;
import java.util.Map;
import javax.validation.Valid;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "idToken",
    "fund",
    "senderCountry",
    "senderCurrency",
    "beneficiaryCountry",
    "beneficiaryCurrency",
    "channel",
    "forexReference"
})
public class Inquiry {

    @JsonProperty("idToken")
    private String idToken;
    @JsonProperty("fund")
    @Valid
    private Fund fund;
    @JsonProperty("senderCountry")
    @Valid
    private SenderCountry senderCountry;
    @JsonProperty("senderCurrency")
    @Valid
    private SenderCurrency senderCurrency;
    @JsonProperty("beneficiaryCountry")
    @Valid
    private BeneficiaryCountry beneficiaryCountry;
    @JsonProperty("beneficiaryCurrency")
    @Valid
    private BeneficiaryCurrency beneficiaryCurrency;
    @JsonProperty("channel")
    @Valid
    private Channel channel;
    @JsonProperty("forexReference")
    @Valid
    private ForexReference forexReference;
    @JsonIgnore
    @Valid
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * No args constructor for use in serialization
     * 
     */
    public Inquiry() {
    }

    /**
     * 
     * @param senderCountry
     * @param forexReference
     * @param fund
     * @param channel
     * @param beneficiaryCurrency
     * @param beneficiaryCountry
     * @param idToken
     * @param senderCurrency
     */
    public Inquiry(String idToken, Fund fund, SenderCountry senderCountry, SenderCurrency senderCurrency, BeneficiaryCountry beneficiaryCountry, BeneficiaryCurrency beneficiaryCurrency, Channel channel, ForexReference forexReference) {
        super();
        this.idToken = idToken;
        this.fund = fund;
        this.senderCountry = senderCountry;
        this.senderCurrency = senderCurrency;
        this.beneficiaryCountry = beneficiaryCountry;
        this.beneficiaryCurrency = beneficiaryCurrency;
        this.channel = channel;
        this.forexReference = forexReference;
    }

    @JsonProperty("idToken")
    public String getIdToken() {
        return idToken;
    }

    @JsonProperty("idToken")
    public void setIdToken(String idToken) {
        this.idToken = idToken;
    }

    public Inquiry withIdToken(String idToken) {
        this.idToken = idToken;
        return this;
    }

    @JsonProperty("fund")
    public Fund getFund() {
        return fund;
    }

    @JsonProperty("fund")
    public void setFund(Fund fund) {
        this.fund = fund;
    }

    public Inquiry withFund(Fund fund) {
        this.fund = fund;
        return this;
    }

    @JsonProperty("senderCountry")
    public SenderCountry getSenderCountry() {
        return senderCountry;
    }

    @JsonProperty("senderCountry")
    public void setSenderCountry(SenderCountry senderCountry) {
        this.senderCountry = senderCountry;
    }

    public Inquiry withSenderCountry(SenderCountry senderCountry) {
        this.senderCountry = senderCountry;
        return this;
    }

    @JsonProperty("senderCurrency")
    public SenderCurrency getSenderCurrency() {
        return senderCurrency;
    }

    @JsonProperty("senderCurrency")
    public void setSenderCurrency(SenderCurrency senderCurrency) {
        this.senderCurrency = senderCurrency;
    }

    public Inquiry withSenderCurrency(SenderCurrency senderCurrency) {
        this.senderCurrency = senderCurrency;
        return this;
    }

    @JsonProperty("beneficiaryCountry")
    public BeneficiaryCountry getBeneficiaryCountry() {
        return beneficiaryCountry;
    }

    @JsonProperty("beneficiaryCountry")
    public void setBeneficiaryCountry(BeneficiaryCountry beneficiaryCountry) {
        this.beneficiaryCountry = beneficiaryCountry;
    }

    public Inquiry withBeneficiaryCountry(BeneficiaryCountry beneficiaryCountry) {
        this.beneficiaryCountry = beneficiaryCountry;
        return this;
    }

    @JsonProperty("beneficiaryCurrency")
    public BeneficiaryCurrency getBeneficiaryCurrency() {
        return beneficiaryCurrency;
    }

    @JsonProperty("beneficiaryCurrency")
    public void setBeneficiaryCurrency(BeneficiaryCurrency beneficiaryCurrency) {
        this.beneficiaryCurrency = beneficiaryCurrency;
    }

    public Inquiry withBeneficiaryCurrency(BeneficiaryCurrency beneficiaryCurrency) {
        this.beneficiaryCurrency = beneficiaryCurrency;
        return this;
    }

    @JsonProperty("channel")
    public Channel getChannel() {
        return channel;
    }

    @JsonProperty("channel")
    public void setChannel(Channel channel) {
        this.channel = channel;
    }

    public Inquiry withChannel(Channel channel) {
        this.channel = channel;
        return this;
    }

    @JsonProperty("forexReference")
    public ForexReference getForexReference() {
        return forexReference;
    }

    @JsonProperty("forexReference")
    public void setForexReference(ForexReference forexReference) {
        this.forexReference = forexReference;
    }

    public Inquiry withForexReference(ForexReference forexReference) {
        this.forexReference = forexReference;
        return this;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public Inquiry withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("idToken", idToken).append("fund", fund).append("senderCountry", senderCountry).append("senderCurrency", senderCurrency).append("beneficiaryCountry", beneficiaryCountry).append("beneficiaryCurrency", beneficiaryCurrency).append("channel", channel).append("forexReference", forexReference).append("additionalProperties", additionalProperties).toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(senderCountry).append(additionalProperties).append(forexReference).append(fund).append(channel).append(beneficiaryCurrency).append(beneficiaryCountry).append(idToken).append(senderCurrency).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof Inquiry) == false) {
            return false;
        }
        Inquiry rhs = ((Inquiry) other);
        return new EqualsBuilder().append(senderCountry, rhs.senderCountry).append(additionalProperties, rhs.additionalProperties).append(forexReference, rhs.forexReference).append(fund, rhs.fund).append(channel, rhs.channel).append(beneficiaryCurrency, rhs.beneficiaryCurrency).append(beneficiaryCountry, rhs.beneficiaryCountry).append(idToken, rhs.idToken).append(senderCurrency, rhs.senderCurrency).isEquals();
    }

}
