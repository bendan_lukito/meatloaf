package com.oriente.meatloaf.model.doku.disbursement;

import java.util.HashMap;
import java.util.Map;
import javax.validation.Valid;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "status",
        "message",
        "remit"
})
public class DokuDisbursementResponse {

    @JsonProperty("status")
    private Integer status;
    @JsonProperty("message")
    private String message;
    @JsonProperty("remit")
    @Valid
    private Remit remit;
    @JsonIgnore
    @Valid
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * No args constructor for use in serialization
     */
    public DokuDisbursementResponse() {
    }

    /**
     * @param message
     * @param status
     * @param remit
     */
    public DokuDisbursementResponse(Integer status, String message, Remit remit) {
        super();
        this.status = status;
        this.message = message;
        this.remit = remit;
    }

    @JsonProperty("status")
    public Integer getStatus() {
        return status;
    }

    @JsonProperty("status")
    public void setStatus(Integer status) {
        this.status = status;
    }

    public DokuDisbursementResponse withStatus(Integer status) {
        this.status = status;
        return this;
    }

    @JsonProperty("message")
    public String getMessage() {
        return message;
    }

    @JsonProperty("message")
    public void setMessage(String message) {
        this.message = message;
    }

    public DokuDisbursementResponse withMessage(String message) {
        this.message = message;
        return this;
    }

    @JsonProperty("remit")
    public Remit getRemit() {
        return remit;
    }

    @JsonProperty("remit")
    public void setRemit(Remit remit) {
        this.remit = remit;
    }

    public DokuDisbursementResponse withRemit(Remit remit) {
        this.remit = remit;
        return this;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public DokuDisbursementResponse withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("status", status).append("message", message).append("remit", remit).append("additionalProperties", additionalProperties).toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(message).append(status).append(additionalProperties).append(remit).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof DokuDisbursementResponse) == false) {
            return false;
        }
        DokuDisbursementResponse rhs = ((DokuDisbursementResponse) other);
        return new EqualsBuilder().append(message, rhs.message).append(status, rhs.status).append(additionalProperties, rhs.additionalProperties).append(remit, rhs.remit).isEquals();
    }

}
