package com.oriente.meatloaf.model.doku.disbursement;

import java.util.HashMap;
import java.util.Map;
import javax.validation.Valid;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "mallId",
        "chainMallId",
        "accountNumber",
        "accountName",
        "channelCode",
        "inquiryId",
        "currency",
        "amount",
        "trxCode",
        "responseCode",
        "responseMsg"
})
public class PaymentData {

    @JsonProperty("mallId")
    private String mallId;
    @JsonProperty("chainMallId")
    private Object chainMallId;
    @JsonProperty("accountNumber")
    private String accountNumber;
    @JsonProperty("accountName")
    private String accountName;
    @JsonProperty("channelCode")
    private String channelCode;
    @JsonProperty("inquiryId")
    private String inquiryId;
    @JsonProperty("currency")
    private String currency;
    @JsonProperty("amount")
    private String amount;
    @JsonProperty("trxCode")
    private String trxCode;
    @JsonProperty("responseCode")
    private String responseCode;
    @JsonProperty("responseMsg")
    private String responseMsg;
    @JsonIgnore
    @Valid
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * No args constructor for use in serialization
     */
    public PaymentData() {
    }

    /**
     * @param amount
     * @param responseCode
     * @param inquiryId
     * @param accountName
     * @param accountNumber
     * @param channelCode
     * @param trxCode
     * @param chainMallId
     * @param mallId
     * @param responseMsg
     * @param currency
     */
    public PaymentData(String mallId, Object chainMallId, String accountNumber, String accountName, String channelCode, String inquiryId, String currency, String amount, String trxCode, String responseCode, String responseMsg) {
        super();
        this.mallId = mallId;
        this.chainMallId = chainMallId;
        this.accountNumber = accountNumber;
        this.accountName = accountName;
        this.channelCode = channelCode;
        this.inquiryId = inquiryId;
        this.currency = currency;
        this.amount = amount;
        this.trxCode = trxCode;
        this.responseCode = responseCode;
        this.responseMsg = responseMsg;
    }

    @JsonProperty("mallId")
    public String getMallId() {
        return mallId;
    }

    @JsonProperty("mallId")
    public void setMallId(String mallId) {
        this.mallId = mallId;
    }

    public PaymentData withMallId(String mallId) {
        this.mallId = mallId;
        return this;
    }

    @JsonProperty("chainMallId")
    public Object getChainMallId() {
        return chainMallId;
    }

    @JsonProperty("chainMallId")
    public void setChainMallId(Object chainMallId) {
        this.chainMallId = chainMallId;
    }

    public PaymentData withChainMallId(Object chainMallId) {
        this.chainMallId = chainMallId;
        return this;
    }

    @JsonProperty("accountNumber")
    public String getAccountNumber() {
        return accountNumber;
    }

    @JsonProperty("accountNumber")
    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public PaymentData withAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
        return this;
    }

    @JsonProperty("accountName")
    public String getAccountName() {
        return accountName;
    }

    @JsonProperty("accountName")
    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public PaymentData withAccountName(String accountName) {
        this.accountName = accountName;
        return this;
    }

    @JsonProperty("channelCode")
    public String getChannelCode() {
        return channelCode;
    }

    @JsonProperty("channelCode")
    public void setChannelCode(String channelCode) {
        this.channelCode = channelCode;
    }

    public PaymentData withChannelCode(String channelCode) {
        this.channelCode = channelCode;
        return this;
    }

    @JsonProperty("inquiryId")
    public String getInquiryId() {
        return inquiryId;
    }

    @JsonProperty("inquiryId")
    public void setInquiryId(String inquiryId) {
        this.inquiryId = inquiryId;
    }

    public PaymentData withInquiryId(String inquiryId) {
        this.inquiryId = inquiryId;
        return this;
    }

    @JsonProperty("currency")
    public String getCurrency() {
        return currency;
    }

    @JsonProperty("currency")
    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public PaymentData withCurrency(String currency) {
        this.currency = currency;
        return this;
    }

    @JsonProperty("amount")
    public String getAmount() {
        return amount;
    }

    @JsonProperty("amount")
    public void setAmount(String amount) {
        this.amount = amount;
    }

    public PaymentData withAmount(String amount) {
        this.amount = amount;
        return this;
    }

    @JsonProperty("trxCode")
    public String getTrxCode() {
        return trxCode;
    }

    @JsonProperty("trxCode")
    public void setTrxCode(String trxCode) {
        this.trxCode = trxCode;
    }

    public PaymentData withTrxCode(String trxCode) {
        this.trxCode = trxCode;
        return this;
    }

    @JsonProperty("responseCode")
    public String getResponseCode() {
        return responseCode;
    }

    @JsonProperty("responseCode")
    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public PaymentData withResponseCode(String responseCode) {
        this.responseCode = responseCode;
        return this;
    }

    @JsonProperty("responseMsg")
    public String getResponseMsg() {
        return responseMsg;
    }

    @JsonProperty("responseMsg")
    public void setResponseMsg(String responseMsg) {
        this.responseMsg = responseMsg;
    }

    public PaymentData withResponseMsg(String responseMsg) {
        this.responseMsg = responseMsg;
        return this;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public PaymentData withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("mallId", mallId).append("chainMallId", chainMallId).append("accountNumber", accountNumber).append("accountName", accountName).append("channelCode", channelCode).append("inquiryId", inquiryId).append("currency", currency).append("amount", amount).append("trxCode", trxCode).append("responseCode", responseCode).append("responseMsg", responseMsg).append("additionalProperties", additionalProperties).toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(responseCode).append(inquiryId).append(channelCode).append(trxCode).append(mallId).append(currency).append(amount).append(accountName).append(accountNumber).append(additionalProperties).append(chainMallId).append(responseMsg).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof PaymentData) == false) {
            return false;
        }
        PaymentData rhs = ((PaymentData) other);
        return new EqualsBuilder().append(responseCode, rhs.responseCode).append(inquiryId, rhs.inquiryId).append(channelCode, rhs.channelCode).append(trxCode, rhs.trxCode).append(mallId, rhs.mallId).append(currency, rhs.currency).append(amount, rhs.amount).append(accountName, rhs.accountName).append(accountNumber, rhs.accountNumber).append(additionalProperties, rhs.additionalProperties).append(chainMallId, rhs.chainMallId).append(responseMsg, rhs.responseMsg).isEquals();
    }

}
