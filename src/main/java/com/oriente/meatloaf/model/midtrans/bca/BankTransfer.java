package com.oriente.meatloaf.model.midtrans.bca;

import java.util.HashMap;
import java.util.Map;
import javax.validation.Valid;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "bank",
        "va_number",
        "free_text",
        "bca"
})
public class BankTransfer {

    @JsonProperty("bank")
    private String bank;
    @JsonProperty("va_number")
    private String vaNumber;
    @JsonProperty("free_text")
    @Valid
    private FreeText freeText;
    @JsonProperty("bca")
    @Valid
    private Bca bca;
    @JsonIgnore
    @Valid
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * No args constructor for use in serialization
     *
     */
    public BankTransfer() {
    }

    /**
     *
     * @param vaNumber
     * @param freeText
     * @param bca
     * @param bank
     */
    public BankTransfer(String bank, String vaNumber, FreeText freeText, Bca bca) {
        super();
        this.bank = bank;
        this.vaNumber = vaNumber;
        this.freeText = freeText;
        this.bca = bca;
    }

    @JsonProperty("bank")
    public String getBank() {
        return bank;
    }

    @JsonProperty("bank")
    public void setBank(String bank) {
        this.bank = bank;
    }

    public BankTransfer withBank(String bank) {
        this.bank = bank;
        return this;
    }

    @JsonProperty("va_number")
    public String getVaNumber() {
        return vaNumber;
    }

    @JsonProperty("va_number")
    public void setVaNumber(String vaNumber) {
        this.vaNumber = vaNumber;
    }

    public BankTransfer withVaNumber(String vaNumber) {
        this.vaNumber = vaNumber;
        return this;
    }

    @JsonProperty("free_text")
    public FreeText getFreeText() {
        return freeText;
    }

    @JsonProperty("free_text")
    public void setFreeText(FreeText freeText) {
        this.freeText = freeText;
    }

    public BankTransfer withFreeText(FreeText freeText) {
        this.freeText = freeText;
        return this;
    }

    @JsonProperty("bca")
    public Bca getBca() {
        return bca;
    }

    @JsonProperty("bca")
    public void setBca(Bca bca) {
        this.bca = bca;
    }

    public BankTransfer withBca(Bca bca) {
        this.bca = bca;
        return this;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public BankTransfer withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("bank", bank).append("vaNumber", vaNumber).append("freeText", freeText).append("bca", bca).append("additionalProperties", additionalProperties).toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(vaNumber).append(additionalProperties).append(freeText).append(bca).append(bank).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof BankTransfer) == false) {
            return false;
        }
        BankTransfer rhs = ((BankTransfer) other);
        return new EqualsBuilder().append(vaNumber, rhs.vaNumber).append(additionalProperties, rhs.additionalProperties).append(freeText, rhs.freeText).append(bca, rhs.bca).append(bank, rhs.bank).isEquals();
    }

}