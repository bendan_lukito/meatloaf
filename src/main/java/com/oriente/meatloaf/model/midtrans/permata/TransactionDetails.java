package com.oriente.meatloaf.model.midtrans.permata;

import java.util.HashMap;
import java.util.Map;
import javax.validation.Valid;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "order_id",
        "gross_amount"
})
public class TransactionDetails {

    @JsonProperty("order_id")
    private String orderId;
    @JsonProperty("gross_amount")
    private Integer grossAmount;
    @JsonIgnore
    @Valid
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * No args constructor for use in serialization
     *
     */
    public TransactionDetails() {
    }

    /**
     *
     * @param grossAmount
     * @param orderId
     */
    public TransactionDetails(String orderId, Integer grossAmount) {
        super();
        this.orderId = orderId;
        this.grossAmount = grossAmount;
    }

    @JsonProperty("order_id")
    public String getOrderId() {
        return orderId;
    }

    @JsonProperty("order_id")
    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public TransactionDetails withOrderId(String orderId) {
        this.orderId = orderId;
        return this;
    }

    @JsonProperty("gross_amount")
    public Integer getGrossAmount() {
        return grossAmount;
    }

    @JsonProperty("gross_amount")
    public void setGrossAmount(Integer grossAmount) {
        this.grossAmount = grossAmount;
    }

    public TransactionDetails withGrossAmount(Integer grossAmount) {
        this.grossAmount = grossAmount;
        return this;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public TransactionDetails withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("orderId", orderId).append("grossAmount", grossAmount).append("additionalProperties", additionalProperties).toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(additionalProperties).append(grossAmount).append(orderId).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof TransactionDetails) == false) {
            return false;
        }
        TransactionDetails rhs = ((TransactionDetails) other);
        return new EqualsBuilder().append(additionalProperties, rhs.additionalProperties).append(grossAmount, rhs.grossAmount).append(orderId, rhs.orderId).isEquals();
    }

}