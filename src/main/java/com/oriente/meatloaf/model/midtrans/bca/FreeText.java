package com.oriente.meatloaf.model.midtrans.bca;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.validation.Valid;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "inquiry",
        "payment"
})
public class FreeText {

    @JsonProperty("inquiry")
    @Valid
    private List<Inquiry> inquiry = null;
    @JsonProperty("payment")
    @Valid
    private List<Payment> payment = null;
    @JsonIgnore
    @Valid
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * No args constructor for use in serialization
     *
     */
    public FreeText() {
    }

    /**
     *
     * @param inquiry
     * @param payment
     */
    public FreeText(List<Inquiry> inquiry, List<Payment> payment) {
        super();
        this.inquiry = inquiry;
        this.payment = payment;
    }

    @JsonProperty("inquiry")
    public List<Inquiry> getInquiry() {
        return inquiry;
    }

    @JsonProperty("inquiry")
    public void setInquiry(List<Inquiry> inquiry) {
        this.inquiry = inquiry;
    }

    public FreeText withInquiry(List<Inquiry> inquiry) {
        this.inquiry = inquiry;
        return this;
    }

    @JsonProperty("payment")
    public List<Payment> getPayment() {
        return payment;
    }

    @JsonProperty("payment")
    public void setPayment(List<Payment> payment) {
        this.payment = payment;
    }

    public FreeText withPayment(List<Payment> payment) {
        this.payment = payment;
        return this;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public FreeText withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("inquiry", inquiry).append("payment", payment).append("additionalProperties", additionalProperties).toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(inquiry).append(payment).append(additionalProperties).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof FreeText) == false) {
            return false;
        }
        FreeText rhs = ((FreeText) other);
        return new EqualsBuilder().append(inquiry, rhs.inquiry).append(payment, rhs.payment).append(additionalProperties, rhs.additionalProperties).isEquals();
    }

}