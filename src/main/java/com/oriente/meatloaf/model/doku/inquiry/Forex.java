
package com.oriente.meatloaf.model.doku.inquiry;

import java.util.HashMap;
import java.util.Map;
import javax.validation.Valid;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "origin",
    "destination"
})
public class Forex {

    @JsonProperty("origin")
    @Valid
    private Origin_ origin;
    @JsonProperty("destination")
    @Valid
    private Destination_ destination;
    @JsonIgnore
    @Valid
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * No args constructor for use in serialization
     * 
     */
    public Forex() {
    }

    /**
     * 
     * @param origin
     * @param destination
     */
    public Forex(Origin_ origin, Destination_ destination) {
        super();
        this.origin = origin;
        this.destination = destination;
    }

    @JsonProperty("origin")
    public Origin_ getOrigin() {
        return origin;
    }

    @JsonProperty("origin")
    public void setOrigin(Origin_ origin) {
        this.origin = origin;
    }

    public Forex withOrigin(Origin_ origin) {
        this.origin = origin;
        return this;
    }

    @JsonProperty("destination")
    public Destination_ getDestination() {
        return destination;
    }

    @JsonProperty("destination")
    public void setDestination(Destination_ destination) {
        this.destination = destination;
    }

    public Forex withDestination(Destination_ destination) {
        this.destination = destination;
        return this;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public Forex withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("origin", origin).append("destination", destination).append("additionalProperties", additionalProperties).toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(additionalProperties).append(origin).append(destination).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof Forex) == false) {
            return false;
        }
        Forex rhs = ((Forex) other);
        return new EqualsBuilder().append(additionalProperties, rhs.additionalProperties).append(origin, rhs.origin).append(destination, rhs.destination).isEquals();
    }

}
