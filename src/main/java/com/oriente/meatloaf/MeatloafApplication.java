package com.oriente.meatloaf;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MeatloafApplication {

	public static void main(String[] args) {
		SpringApplication.run(MeatloafApplication.class, args);
	}
}
