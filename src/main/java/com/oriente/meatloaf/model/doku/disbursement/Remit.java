package com.oriente.meatloaf.model.doku.disbursement;

import java.util.HashMap;
import java.util.Map;
import javax.validation.Valid;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "paymentData",
        "transactionId"
})
public class Remit {

    @JsonProperty("paymentData")
    @Valid
    private PaymentData paymentData;
    @JsonProperty("transactionId")
    private String transactionId;
    @JsonIgnore
    @Valid
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * No args constructor for use in serialization
     *
     */
    public Remit() {
    }

    /**
     *
     * @param transactionId
     * @param paymentData
     */
    public Remit(PaymentData paymentData, String transactionId) {
        super();
        this.paymentData = paymentData;
        this.transactionId = transactionId;
    }

    @JsonProperty("paymentData")
    public PaymentData getPaymentData() {
        return paymentData;
    }

    @JsonProperty("paymentData")
    public void setPaymentData(PaymentData paymentData) {
        this.paymentData = paymentData;
    }

    public Remit withPaymentData(PaymentData paymentData) {
        this.paymentData = paymentData;
        return this;
    }

    @JsonProperty("transactionId")
    public String getTransactionId() {
        return transactionId;
    }

    @JsonProperty("transactionId")
    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public Remit withTransactionId(String transactionId) {
        this.transactionId = transactionId;
        return this;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public Remit withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("paymentData", paymentData).append("transactionId", transactionId).append("additionalProperties", additionalProperties).toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(transactionId).append(paymentData).append(additionalProperties).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof Remit) == false) {
            return false;
        }
        Remit rhs = ((Remit) other);
        return new EqualsBuilder().append(transactionId, rhs.transactionId).append(paymentData, rhs.paymentData).append(additionalProperties, rhs.additionalProperties).isEquals();
    }

}
