
package com.oriente.meatloaf.model.doku.disbursement;

import java.util.HashMap;
import java.util.Map;
import javax.validation.Valid;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "address",
    "birthDate",
    "country",
    "firstName",
    "gender",
    "lastName",
    "personalId",
    "personalIdCountry",
    "personalIdExpireDate",
    "personalIdIssueDate",
    "personalIdType",
    "phoneNumber"
})
public class Sender {

    @JsonProperty("address")
    private String address;
    @JsonProperty("birthDate")
    private String birthDate;
    @JsonProperty("country")
    @Valid
    private Country_ country;
    @JsonProperty("firstName")
    private String firstName;
    @JsonProperty("gender")
    private String gender;
    @JsonProperty("lastName")
    private String lastName;
    @JsonProperty("personalId")
    private String personalId;
    @JsonProperty("personalIdCountry")
    @Valid
    private PersonalIdCountry personalIdCountry;
    @JsonProperty("personalIdExpireDate")
    private String personalIdExpireDate;
    @JsonProperty("personalIdIssueDate")
    private String personalIdIssueDate;
    @JsonProperty("personalIdType")
    private String personalIdType;
    @JsonProperty("phoneNumber")
    private String phoneNumber;
    @JsonIgnore
    @Valid
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * No args constructor for use in serialization
     * 
     */
    public Sender() {
    }

    /**
     * 
     * @param personalIdType
     * @param lastName
     * @param personalIdCountry
     * @param phoneNumber
     * @param personalIdIssueDate
     * @param address
     * @param personalId
     * @param gender
     * @param personalIdExpireDate
     * @param birthDate
     * @param firstName
     * @param country
     */
    public Sender(String address, String birthDate, Country_ country, String firstName, String gender, String lastName, String personalId, PersonalIdCountry personalIdCountry, String personalIdExpireDate, String personalIdIssueDate, String personalIdType, String phoneNumber) {
        super();
        this.address = address;
        this.birthDate = birthDate;
        this.country = country;
        this.firstName = firstName;
        this.gender = gender;
        this.lastName = lastName;
        this.personalId = personalId;
        this.personalIdCountry = personalIdCountry;
        this.personalIdExpireDate = personalIdExpireDate;
        this.personalIdIssueDate = personalIdIssueDate;
        this.personalIdType = personalIdType;
        this.phoneNumber = phoneNumber;
    }

    @JsonProperty("address")
    public String getAddress() {
        return address;
    }

    @JsonProperty("address")
    public void setAddress(String address) {
        this.address = address;
    }

    public Sender withAddress(String address) {
        this.address = address;
        return this;
    }

    @JsonProperty("birthDate")
    public String getBirthDate() {
        return birthDate;
    }

    @JsonProperty("birthDate")
    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public Sender withBirthDate(String birthDate) {
        this.birthDate = birthDate;
        return this;
    }

    @JsonProperty("country")
    public Country_ getCountry() {
        return country;
    }

    @JsonProperty("country")
    public void setCountry(Country_ country) {
        this.country = country;
    }

    public Sender withCountry(Country_ country) {
        this.country = country;
        return this;
    }

    @JsonProperty("firstName")
    public String getFirstName() {
        return firstName;
    }

    @JsonProperty("firstName")
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public Sender withFirstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    @JsonProperty("gender")
    public String getGender() {
        return gender;
    }

    @JsonProperty("gender")
    public void setGender(String gender) {
        this.gender = gender;
    }

    public Sender withGender(String gender) {
        this.gender = gender;
        return this;
    }

    @JsonProperty("lastName")
    public String getLastName() {
        return lastName;
    }

    @JsonProperty("lastName")
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Sender withLastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    @JsonProperty("personalId")
    public String getPersonalId() {
        return personalId;
    }

    @JsonProperty("personalId")
    public void setPersonalId(String personalId) {
        this.personalId = personalId;
    }

    public Sender withPersonalId(String personalId) {
        this.personalId = personalId;
        return this;
    }

    @JsonProperty("personalIdCountry")
    public PersonalIdCountry getPersonalIdCountry() {
        return personalIdCountry;
    }

    @JsonProperty("personalIdCountry")
    public void setPersonalIdCountry(PersonalIdCountry personalIdCountry) {
        this.personalIdCountry = personalIdCountry;
    }

    public Sender withPersonalIdCountry(PersonalIdCountry personalIdCountry) {
        this.personalIdCountry = personalIdCountry;
        return this;
    }

    @JsonProperty("personalIdExpireDate")
    public String getPersonalIdExpireDate() {
        return personalIdExpireDate;
    }

    @JsonProperty("personalIdExpireDate")
    public void setPersonalIdExpireDate(String personalIdExpireDate) {
        this.personalIdExpireDate = personalIdExpireDate;
    }

    public Sender withPersonalIdExpireDate(String personalIdExpireDate) {
        this.personalIdExpireDate = personalIdExpireDate;
        return this;
    }

    @JsonProperty("personalIdIssueDate")
    public String getPersonalIdIssueDate() {
        return personalIdIssueDate;
    }

    @JsonProperty("personalIdIssueDate")
    public void setPersonalIdIssueDate(String personalIdIssueDate) {
        this.personalIdIssueDate = personalIdIssueDate;
    }

    public Sender withPersonalIdIssueDate(String personalIdIssueDate) {
        this.personalIdIssueDate = personalIdIssueDate;
        return this;
    }

    @JsonProperty("personalIdType")
    public String getPersonalIdType() {
        return personalIdType;
    }

    @JsonProperty("personalIdType")
    public void setPersonalIdType(String personalIdType) {
        this.personalIdType = personalIdType;
    }

    public Sender withPersonalIdType(String personalIdType) {
        this.personalIdType = personalIdType;
        return this;
    }

    @JsonProperty("phoneNumber")
    public String getPhoneNumber() {
        return phoneNumber;
    }

    @JsonProperty("phoneNumber")
    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public Sender withPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
        return this;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public Sender withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("address", address).append("birthDate", birthDate).append("country", country).append("firstName", firstName).append("gender", gender).append("lastName", lastName).append("personalId", personalId).append("personalIdCountry", personalIdCountry).append("personalIdExpireDate", personalIdExpireDate).append("personalIdIssueDate", personalIdIssueDate).append("personalIdType", personalIdType).append("phoneNumber", phoneNumber).append("additionalProperties", additionalProperties).toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(personalIdType).append(lastName).append(personalIdCountry).append(personalIdIssueDate).append(country).append(phoneNumber).append(address).append(additionalProperties).append(personalId).append(gender).append(personalIdExpireDate).append(birthDate).append(firstName).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof Sender) == false) {
            return false;
        }
        Sender rhs = ((Sender) other);
        return new EqualsBuilder().append(personalIdType, rhs.personalIdType).append(lastName, rhs.lastName).append(personalIdCountry, rhs.personalIdCountry).append(personalIdIssueDate, rhs.personalIdIssueDate).append(country, rhs.country).append(phoneNumber, rhs.phoneNumber).append(address, rhs.address).append(additionalProperties, rhs.additionalProperties).append(personalId, rhs.personalId).append(gender, rhs.gender).append(personalIdExpireDate, rhs.personalIdExpireDate).append(birthDate, rhs.birthDate).append(firstName, rhs.firstName).isEquals();
    }

}
