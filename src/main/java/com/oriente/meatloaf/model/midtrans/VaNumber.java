package com.oriente.meatloaf.model.midtrans;

import java.util.HashMap;
import java.util.Map;
import javax.validation.Valid;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "bank",
        "va_number"
})
public class VaNumber {

    @JsonProperty("bank")
    private String bank;
    @JsonProperty("va_number")
    private String vaNumber;
    @JsonIgnore
    @Valid
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * No args constructor for use in serialization
     *
     */
    public VaNumber() {
    }

    /**
     *
     * @param vaNumber
     * @param bank
     */
    public VaNumber(String bank, String vaNumber) {
        super();
        this.bank = bank;
        this.vaNumber = vaNumber;
    }

    @JsonProperty("bank")
    public String getBank() {
        return bank;
    }

    @JsonProperty("bank")
    public void setBank(String bank) {
        this.bank = bank;
    }

    public VaNumber withBank(String bank) {
        this.bank = bank;
        return this;
    }

    @JsonProperty("va_number")
    public String getVaNumber() {
        return vaNumber;
    }

    @JsonProperty("va_number")
    public void setVaNumber(String vaNumber) {
        this.vaNumber = vaNumber;
    }

    public VaNumber withVaNumber(String vaNumber) {
        this.vaNumber = vaNumber;
        return this;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public VaNumber withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("bank", bank).append("vaNumber", vaNumber).append("additionalProperties", additionalProperties).toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(vaNumber).append(additionalProperties).append(bank).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof VaNumber) == false) {
            return false;
        }
        VaNumber rhs = ((VaNumber) other);
        return new EqualsBuilder().append(vaNumber, rhs.vaNumber).append(additionalProperties, rhs.additionalProperties).append(bank, rhs.bank).isEquals();
    }

}